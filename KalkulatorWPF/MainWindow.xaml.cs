﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace KalkulatorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double liczba1, liczba2;
        Dzialania dzialanie;
        bool drugaLiczba = false;
        bool wpisanoLiczbe = false;
        bool przecinek = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        public enum Dzialania { Dodawanie = 1, Odejmowanie, Mnozenie, Dzielenie, Pierwiastek, LiczbaOdwrotna };

        private double Dodawanie(double a, double b)
        {
            return a + b;
        }

        private double Odejmowanie(double a, double b)
        {
            return a - b;
        }

        private double Mnozenie(double a, double b)
        {
            return a * b;
        }

        private double Dzielenie(double a, double b)
        {
            if(b!=0)
            {
                return a / b;
            }
            else
            {
                MessageBox.Show("Nie dzielimy przez 0.");
                return 0;
            }
        }

        private double Pierwiastek(double a)
        {
            if (a >= 0)
            {
                return Math.Sqrt(a);
            }
            else
            {
                MessageBox.Show("Nie pieriwastkujemy liczb ujemnych.");
                return -1;
            }
        }

        private double LiczbaOdwrotna(double a)
        {
            if(a!=0)
            {
                return 1 / a;
            }
            else
            {
                MessageBox.Show("Nie dzielimy przez 0.");
                return 0;
            }
        }

        private void buttonLiczba_Click(object sender, RoutedEventArgs e)
        {
            var zrodlo = sender as Button;
            string wpisana_wartosc = "";

            if (zrodlo != null)
            {
                wpisana_wartosc = zrodlo.Content.ToString();
            }
            textBox1.Text += wpisana_wartosc;
            wpisanoLiczbe = true;
        }

        private void buttonDecSep_Click(object sender, RoutedEventArgs e)
        {
            if (!przecinek)
            {
                textBox1.Text += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                przecinek = true;
            }
        }

        private void button17_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = "";
            przecinek = false;
            drugaLiczba = false;
            wpisanoLiczbe = false;

            button1.IsEnabled = true;
            button2.IsEnabled = true;
            button3.IsEnabled = true;
            button4.IsEnabled = true;
            button5.IsEnabled =true;
            button6.IsEnabled = true;
            button7.IsEnabled = true;
            button8.IsEnabled = true;
            button10.IsEnabled = true;
            button11.IsEnabled = true;
            button13.IsEnabled = true;
            button14.IsEnabled = true;
            button15.IsEnabled = true;
            button16.IsEnabled = true;
            button18.IsEnabled = true;
            button19.IsEnabled = true;
            buttonCalc.IsEnabled = true;
            buttonDecimalSep.IsEnabled = true;
        }

        private void buttonDzialanie_Click(object sender, RoutedEventArgs e)
        {
            var zrodlo = sender as Button;
            string dzialanie_s = "";

            wpisz_liczbe(textBox1.Text);

            if (zrodlo != null)
            {
                dzialanie_s = zrodlo.Content.ToString();

                if (dzialanie_s == "+")
                {
                    dzialanie = Dzialania.Dodawanie;
                    //textBox1.Text += dzialanie_s;
                }
                if (dzialanie_s == "-")
                {
                    if (wpisanoLiczbe)
                    {
                        dzialanie = Dzialania.Odejmowanie;
                    }
                    else
                    {
                        textBox1.Text += dzialanie_s;
                        drugaLiczba = false;
                    }
                }
                if (dzialanie_s == "*")
                {
                    dzialanie = Dzialania.Mnozenie;
                    //textBox1.Text += dzialanie_s;
                }
                if (dzialanie_s == "/")
                {
                    dzialanie = Dzialania.Dzielenie;
                    //textBox1.Text += dzialanie_s;
                }
                if (dzialanie_s == "√")
                {
                    dzialanie = Dzialania.Pierwiastek;
                    string tmp = textBox1.Text;
                    //textBox1.Text = "√" + tmp;
                }
                if (dzialanie_s == "1/x")
                {
                    dzialanie = Dzialania.LiczbaOdwrotna;
                    string tmp = textBox1.Text;
                    //textBox1.Text = "1/" + tmp;
                }

                drugaLiczba = true;
                wpisanoLiczbe = false;
                textBox1.Text = String.Empty;
            }
        }

        private void wpisz_liczbe(string zapis)
        {
            if (!drugaLiczba)
            {
                bool czyOK = Double.TryParse(textBox1.Text, out liczba1);
            }
            else
            {
                bool czyOK = Double.TryParse(textBox1.Text, out liczba2);
            }
        }

        private void buttonCalc_Click(object sender, RoutedEventArgs e)
        {
            //if (drugaLiczba)
            //{
            //    if (!textBox1.Text.StartsWith("-"))
            //    {
            //        string[] liczby = textBox1.Text.Split(new string[] { "+", "-", "*", "/", "√", "1/" }, StringSplitOptions.RemoveEmptyEntries);
            //        bool czyOK = Double.TryParse(liczby[0], out liczba1);
            //        if (liczby.Length > 1)
            //            czyOK = Double.TryParse(liczby[1], out liczba2);
            //    }
            //    else
            //    {
            //        string wzor = @"\s+-\d+";
            //        Regex.IsMatch(textBox1.Text, wzor);
            //    }
            //}
            wpisz_liczbe(textBox1.Text);

            button1.IsEnabled = false;
            button2.IsEnabled = false;
            button3.IsEnabled = false;
            button4.IsEnabled = false;
            button5.IsEnabled = false;
            button6.IsEnabled = false;
            button7.IsEnabled = false;
            button8.IsEnabled = false;
            button10.IsEnabled = false;
            button11.IsEnabled = false;
            button13.IsEnabled = false;
            button14.IsEnabled = false;
            button15.IsEnabled = false;
            button16.IsEnabled = false;
            button18.IsEnabled = false;
            button19.IsEnabled = false;
            buttonCalc.IsEnabled = false;
            buttonDecimalSep.IsEnabled = false;

            textBox1.Text += "=";

            if (dzialanie == Dzialania.Dodawanie)
            {
                textBox1.Text=
                Dodawanie(liczba1, liczba2).ToString();
            }

            if (dzialanie == Dzialania.Odejmowanie)
            {
                textBox1.Text =
                Odejmowanie(liczba1, liczba2).ToString();
            }

            if (dzialanie == Dzialania.Mnozenie)
            {
                textBox1.Text =
                Mnozenie(liczba1, liczba2).ToString();
            }

            if (dzialanie == Dzialania.Dzielenie)
            {
                textBox1.Text =
                Dzielenie(liczba1, liczba2).ToString();
            }

            if (dzialanie == Dzialania.Pierwiastek)
            {
                textBox1.Text =
                Pierwiastek(liczba1).ToString();
            }

            if (dzialanie == Dzialania.LiczbaOdwrotna)
            {
                textBox1.Text =
                LiczbaOdwrotna(liczba1).ToString();
            }

            drugaLiczba = false;
        }
    }
}
